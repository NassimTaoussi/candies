import 'package:flutter/material.dart';

var foo = 0;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override

  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "ArchitectsDaughter"),
      home: DynamicChange(),
    );
  }
}

class DynamicChange extends StatefulWidget {
  const DynamicChange({Key? key}) : super(key: key);
  @override
  _StateDynamic createState() => _StateDynamic();
}

class _StateDynamic extends State<DynamicChange> {
  // This widget is the root of your application.
  @override

  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "ArchitectsDaughter"),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Candy", style: TextStyle(color: Color(0xFF880E4F)),),
          backgroundColor: const Color(0xFFF06292),
        ),
        body: Center(
          child: Column(
            children: [
              const Image( image: AssetImage("assets/images/candy.png"),
                height: 180,
                width: 180,
                fit: BoxFit.cover,
              ),
              const Text("How candies have you eat today ?", style: TextStyle(fontSize:20), textAlign: TextAlign.center),
              Container(
                alignment: Alignment.center,
                child: Text("$foo", style: const TextStyle(fontSize: 130), ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add), 
          tooltip: 'Increment Counter',
          onPressed: () => setState(() {
            foo++;
          }),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
